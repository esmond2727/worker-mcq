package util

import (
	"context"
	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
)

var BgCtx = context.Background()

func SetupRedisClient() *redis.Client {
	address := GetEnvDefault("REDIS_HOST", "localhost")
	port := GetEnvDefault("REDIS_PORT", "6379")
	rdb := redis.NewClient(&redis.Options{
		Addr:     address + ":" + port,
		Password: GetEnvDefault("REDIS_PASSWORD", ""),
		DB:       0,
	})

	pong, err := rdb.Ping(BgCtx).Result()
	log.Println(pong, err)
	return rdb
}
